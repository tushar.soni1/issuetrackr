import { WebClient } from '@slack/web-api';
import { SLACK_AUTH_TOKEN } from './config.js';
const slackClient = new WebClient(SLACK_AUTH_TOKEN);
import express, { json } from 'express';
const app = express();
const port = 3000;

app.use(json());

app.post('/mention', (req, res) => {
  const payload = req.body;
  const event = payload.event;

  if (event && event.type === 'app_mention') {
    const user = event.user;
    const text = event.text;
    const channel = event.channel;
    const format = text.match(/<@[^>]+>\s*(.*)/);
    const message = format ? format[1] : '';
    console.log("The text message is : " , message)
    console.log("The Thread id is : " ,event.event_ts )
  }
  res.status(200).json("success")
});

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});

